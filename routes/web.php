<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

/*
Route::get('/', function () {
    return view('welcome');
});
*/

//以下順番注意
Route::group(['middleware' => ['web']], function(){
	Route::get('/create','ArticlesController@create');			//新規
	Route::post('/store','ArticlesController@store');			//新規保存
	Route::get('/edit/{id}','ArticlesController@edit');			//編集
	Route::patch('/edit/{id}','ArticlesController@update');		//編集保存
	Route::delete('/delete/{id}','ArticlesController@destroy');	//削除
	Route::get('/{id}','ArticlesController@show');				//詳細
	Route::get('/','ArticlesController@index');					//一覧
});


