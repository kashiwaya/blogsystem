<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;	//<-Modelをuseしておくとメソッドの記述が簡潔に。

class ArticlesController extends Controller
{
    //一覧表示
	public function index(){
		$articles = Article::all();
		return view('articles.index')->with('articles',$articles);
	}
	
	//詳細表示
	public function show($id){
		$article = Article::findOrFail($id);
		return view('articles.detail')->with('article',$article);		
	}
	
	//削除
	public function destroy($id){
		$article = Article::findOrFail($id);
		$article->delete();
		return redirect('/')->with('flash_message','記事を削除しました。');
	}
	
	//新規登録View表示
	public function create(){
		return view('articles.create');
		
	}
	//新規登録POST
	public function store(Request $ar){
		$article = new Article();
		$article->title = $ar->title;
		$article->body = $ar->body;
		$article->save();
		return redirect('/')->with('flash_message','新しい記事が登録されました。');
	}
	
	//編集
	public function edit($id){
		$article = Article::findOrFail($id);
		return view('articles.edit')->with('article',$article);
	}
	public function update(Request $pr,$id){
		$article = Article::findOrFail($id);
		$article->title = $pr->title;
		$article->body = $pr->body;
		$article->save();
		return redirect('/'.$id)->with('flash_message','編集完了');
	}
}
