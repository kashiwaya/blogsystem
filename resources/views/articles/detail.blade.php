@extends('layouts.common')

@section('content')
<div class="container">
	<h1>記事詳細画面</h1>
	<hr>
@if(Session::get('flash_message'))
	<div>{{ session('flash_message') }}</div>
@endif
	<h2>{{ $article->title }}</h2>
	<hr>
	<p>{{ $article->body }}</p>
	<form action="/delete/{{ $article->id }}" method="post">
		{{ csrf_field() }}
		{{ method_field('delete') }}
		<input type="submit" value="Delete"> 
	</form>
	<a href="http://127.0.0.1:8000/edit/{{ $article->id }}">編集</a>
	<hr>
</div>
@endsection