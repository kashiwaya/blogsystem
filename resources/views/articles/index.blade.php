@extends('layouts.common')

@section('content')

<div class="container">
	<h1>トップページ</h1>
	<hr>
	<div>
		<a href="http://127.0.0.1:8000/create">新規登録</a>
	</div>
	<hr>
@if(Session::get('flash_message'))
	<div style="margin:5px 0px 5px 0px;">{{ session('flash_message') }}</div>
	<hr>
@endif

@foreach($articles as $article)
	<div>
		<a href="http://127.0.0.1:8000/{{ $article->id }}">{{ $article->title }}</a>
		<p>{{ $article->body }}</p>
	</div>
	<hr>
@endforeach
</div>

@endsection
