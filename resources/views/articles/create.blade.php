@extends('layouts.common')

@section('content')
<div class="container">
	<h1>新規記事登録画面</h1>
	<hr>
	<form action="/store" method="post">
		{{ csrf_field() }}
		<p>タイトル</p>
		<input type="text" name="title">
		<p>本文</p>
		<textarea name="body"></textarea>
		<div style="margin-top: 50px;">
			<input type="submit" value="登録">
		</div>
	</form>
</div>
@endsection