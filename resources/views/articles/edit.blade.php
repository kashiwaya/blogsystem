@extends('layouts.common')

@section('content')
<div class="container">
	<h1>記事編集画面</h1>
	<hr>
	<form action="/edit/{{ $article->id }}" method="post">
		{{ csrf_field() }}
		{{ method_field('patch') }}
		<p>タイトル</p>
		<input type="text" name="title" value="{{ old('title',$article->title) }}"><br>
		<p>本文</p>
		<textarea name="body">{{ old('body',$article->body) }}</textarea>
		<div style="margin-top: 50px;">
			<input type="submit" value=" 更新 ">
		</div>
	</form>
</div>
@endsection